package com.mmnytimesapp.api.interfaces;

import com.mmnytimesapp.api.models.TopStories;
import com.mmnytimesapp.api.models.UserContent;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Query;
import rx.Observable;

public interface INYTimes {
    @Headers("Content-Type: text/json")
    @GET("/topstories/v2/home.json")
    void getTopStories(Callback<TopStories> callback);

    @Headers("Content-Type: text/json")
    @GET("/topstories/v2/home.json")
    Observable<TopStories> getTopStoriesObservable();

    @Headers("Content-Type: text/json")
    @GET("/community/v3/user-content/url.json")
    void getUserContentForUrl(@Query("url") String url, Callback<UserContent> callback);

    @Headers("Content-Type: text/json")
    @GET("/community/v3/user-content/url.json")
    UserContent getUserContentForUrl_noCallback(@Query("url") String url);


    @Headers("Content-Type: text/json")
    @GET("/community/v3/user-content/url.json")
    Observable<UserContent> getUserContentForUrlObservable(@Query("url") String url);
}
