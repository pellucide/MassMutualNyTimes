package com.mmnytimesapp.api.models;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 Sample json for the call
 {
   "debug": {
     "version": 3.1
   },
   "status": "OK",
   "copyright": "Copyright (c) 2016 The New York Times Company.  All Rights Reserved.",
   "results": {
     "comments": [
     {
       "commentID": 18912646,
       "status": "approved",
       "commentSequence": 18912646,
       "userID": 48000580,
       "userDisplayName": "John",
       "userLocation": "Hartford",
       "userTitle": "NULL",
       "userURL": "NULL",
       "commentTitle": "<br/>",
       "commentBody": "You can't unless you want to ban free thinking. Any more than you can stop a madman like the killer at Newtown. Leaving aside the fact that people like Mateen are mad by definition. Actually what Comey said was that the real difficulty was finding pieces of hay in the nationwide haystack that may turn into needles. An infinitely greater challenge than looking for needles.",
       "createDate": "1466596301",
       "updateDate": "1466596327",
       "approveDate": "1466596327",
       "recommendations": 4,
       "replyCount": 0,
       "replies": [],
       "editorsSelection": false,
       "parentID": null,
       "parentUserDisplayName": null,
       "depth": 1,
       "commentType": "comment",
       "trusted": 1,
       "recommendedFlag": null,
       "reportAbuseFlag": null,
       "permID": "18912646",
       "picURL": "https://s3.amazonaws.com/pimage.timespeople.nytimes.com/4800/0580/cropped-48000580.jpg?0.8383151208239556",
       "timespeople": 1,
       "sharing": 0
     }],
     "page": 1,
     "totalCommentsReturned": 1,
     "totalCommentsFound": 1,
     "totalParentCommentsFound": 1,
     "totalParentCommentsReturned": 1,
     "totalReplyCommentsFound": 0,
     "totalReplyCommentsReturned": 0,
     "totalReporterReplyCommentsFound": 0,
     "totalReporterReplyCommentsReturned": 0,
     "totalEditorsSelectionFound": 0,
     "totalEditorsSelectionReturned": 0,
     "totalRecommendationsFound": 1,
     "totalRecommendationsReturned": 1,
     "replyLimit": 3,
     "depthLimit": 0,
     "sort": "oldest",
     "filter": "",
     "callerID": null,
     "api_timestamp": "1466597894"
   }
 }
 */
public class UserContent {

    @SerializedName("status")
    private String mStatus;
    public String getStatus() {
        return mStatus;
    }

    @SerializedName("copyright")
    private String mCopyright;
    public String getCopyright() {
        return mCopyright;
    }

    @SerializedName("results")
    private Result mResult;
    public Result getResult() {
        return mResult;
    }

    public class Result {
        @SerializedName("comments")
        public List<Comment> comments;

        @SerializedName("api_timestamp")
        private String mTimeStamp;
        public String getTimeStamp() {
            return mTimeStamp;
        }
        public int page;//"page": 1,
        public int totalCommentsReturned;// "totalCommentsReturned": 1,
        public int totalCommentsFound;// "totalCommentsFound": 1,
        public int totalParentCommentsFound;// "totalParentCommentsFound": 1,
        public int totalParentCommentsReturned;// "totalParentCommentsReturned": 1,
        public int totalReplyCommentsFound;// "totalReplyCommentsFound": 0,
        public int totalReplyCommentsReturned;// "totalReplyCommentsReturned": 0,
        public int totalReporterReplyCommentsFound;// "totalReporterReplyCommentsFound": 0,
        public int totalReporterReplyCommentsReturned;// "totalReporterReplyCommentsReturned": 0,
        public int totalEditorsSelectionFound;// "totalEditorsSelectionFound": 0,
        public int totalEditorsSelectionReturned;// "totalEditorsSelectionReturned": 0,
        public int totalRecommendationsFound;// "totalRecommendationsFound": 1,
        public int totalRecommendationsReturned;// "totalRecommendationsReturned": 1,
        public int replyLimit;// "replyLimit": 3,
        public int depthLimit;// "depthLimit": 0,
        public String sort;// "sort": "oldest",
        public String filter;// "filter": "",
        public int callerI;// "callerID": null,
    }

    public class Comment {
        public class Reply {
            public int commentID; // "commentID": 18912646,
            public String status;// "status": "approved",
            public int userID; //"userID": 48000580,
        }
        public int commentID; // "commentID": 18912646,
        public String status;// "status": "approved",
        public int commentSequence; //"commentSequence": 18912646,
        public int userID; //"userID": 48000580,
        public String userDisplayName; //"userDisplayName": "John",
        public String userlocation; //"userLocation": "Hartford",
        public String usertitle; //"userTitle": "NULL",
        public String userURL; //"userURL": "NULL",
        public String commentTitle; //"commentTitle": "<br/>",
        public String commentBody; //"commentBody": "You can't unless you want to ban free thinking. Any more than you can stop a madman like the killer at Newtown. Leaving aside the fact that people like Mateen are mad by definition. Actually what Comey said was that the real difficulty was finding pieces of hay in the nationwide haystack that may turn into needles. An infinitely greater challenge than looking for needles.",
        public String createDate; //"createDate": "1466596301",
        public String updateDate; //"updateDate": "1466596327",
        public String approveDate; //"approveDate": "1466596327",
        public int recommendations; // "recommendations": 4,
        public int replyCount; //"replyCount": 0,
        public Reply[] replies;// "replies": [],
        public boolean editorsSelection;//"editorsSelection": false,
        public int parentID;//"parentID": null,
        public String parentUserDisplayName;//"parentUserDisplayName": null,
        public int depth;//"depth": 1,
        public String commentType;//"commentType": "comment",
        public int trusted; //"trusted": 1,
        public boolean recommendedFlag;//"recommendedFlag": null,
        public boolean reportAbuseFlag;//"reportAbuseFlag": null,
        public String permID;//"permID": "18912646",
        public String picURL;//"picURL": "https://s3.amazonaws.com/pimage.timespeople.nytimes.com/4800/0580/cropped-48000580.jpg?0.8383151208239556",
        public int timespeople; //"timespeople": 1,
        public int sharing; //"sharing": 0
    }

}
