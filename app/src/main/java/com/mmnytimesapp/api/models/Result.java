package com.mmnytimesapp.api.models;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Result {
    static String[] date_formats = new String[] {
            //"yyyy-MM-dd",
            //"yyyy-MM-dd HH:mm",
            //"yyyy-MM-dd HH:mmZ",
            //"yyyy-MM-dd HH:mm:ss.SSSZ",
            "yyyy-MM-dd'T'HH:mm:ssZ",
            "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
            //exammple "2016-06-26T05:21:08-04:00",
    };

    public Result(String title, Multimedia ... multimedia) {
        this.mTitle = title.replace("\"", "");;
        mMultimedia = Arrays.asList(multimedia);
    }

    public static Date getDate(String dateStr) {
        Date date = null;
         for (String format:date_formats) {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
            try {
                date = sdf.parse(dateStr);
            } catch (ParseException e) {
                e.printStackTrace();
                continue;
            }
            break;
        }
        return date;
    }

    public Result(String title, String section, String createdDate,  Multimedia ... multimedia) {
        this.mTitle = title.replace("\"", "");;
        mMultimedia = Arrays.asList(multimedia);
        mSection = section;
        mCreatedDate = getDate(createdDate);
    }

    @SerializedName("section")
    private String mSection;
    public String getSection() {
        return mSection;
    }

    @SerializedName("subsection")
    private String mSubSection;
    public String getSubSection() {
        return mSubSection;
    }

    @SerializedName("title")
    private String mTitle;
    public String getTitle() {
        return mTitle;
    }

    @SerializedName("abstract")
    private String mAbstract;
    public String getAbstract() {
        return mAbstract;
    }

    @SerializedName("url")
    private String mUrl;
    public String getUrl() {
        return mUrl;
    }

    @SerializedName("byline")
    private String myByLine;
    public String getMyByLine() {
        return myByLine;
    }

    @SerializedName("item_type")
    private String mItemType;
    public String getItemType() {
        return mItemType;
    }

    @SerializedName("updated_date")
    private Date mUpdatedDate;
    public Date getUpdatedDate() {
        return mUpdatedDate;
    }

    @SerializedName("created_date")
    private Date mCreatedDate;
    public Date getCreatedDate() {
        return mCreatedDate;
    }

    @SerializedName("multimedia")
    private List<Multimedia> mMultimedia;
    public List<Multimedia> getMultimedia() {
        return mMultimedia;
    }

    public void setmSection(String mSection) {
        this.mSection = mSection;
    }

    public void setmSubSection(String mSubSection) {
        this.mSubSection = mSubSection;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setmAbstract(String mAbstract) {
        this.mAbstract = mAbstract;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public void setMyByLine(String myByLine) {
        this.myByLine = myByLine;
    }

    public void setmItemType(String mItemType) {
        this.mItemType = mItemType;
    }

    public void setmUpdatedDate(String mUpdatedDate) {
        this.mUpdatedDate = getDate(mUpdatedDate);
    }

    public void setmCreatedDate(String mCreatedDate) {
        this.mCreatedDate = getDate(mCreatedDate);
    }

    public void setmMultimedia(List<Multimedia> mMultimedia) { this.mMultimedia = mMultimedia; }

    public UserContent mUserContent;
}
