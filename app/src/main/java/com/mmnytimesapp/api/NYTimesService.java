package com.mmnytimesapp.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mmnytimesapp.api.interfaces.INYTimes;
import com.mmnytimesapp.api.models.Result;
import com.mmnytimesapp.api.models.TopStories;
import com.mmnytimesapp.api.models.UserContent;
import com.mmnytimesapp.api.utils.ResultsDeserializerJson;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import rx.Observable;

public class NYTimesService {
    private static final String API_URL = "http://api.nytimes.com/svc";
    private static final String API_KEY1 = "be33c600f7144d0a88b2a5f6ec4cdba3"; //API Key for the Top Stories V1 and V2
    private static final String API_KEY2 = "7227600a59795c72dac910e8219140a7:7:74255139"; // API key from MM
    private static final String API_KEY = API_KEY1;

    private static NYTimesService mNYTimesService;
    private static RestAdapter mAsyncRestAdapter;
    private static INYTimes nyTimes;

    public static NYTimesService getInstance() {
        if (mNYTimesService == null)
            mNYTimesService = new NYTimesService();
        return mNYTimesService;
    }

    private NYTimesService() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Result.class, new ResultsDeserializerJson()).create();

        mAsyncRestAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setConverter(new GsonConverter(gson))
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addEncodedQueryParam("api-key", API_KEY);
                    }
                })
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build();
    }

    private void init() {
        if (nyTimes == null)
            nyTimes = mAsyncRestAdapter.create(INYTimes.class);
    }

    public void getTopStories(Callback<TopStories> callback) {
        init();
        nyTimes.getTopStories(callback);
    }

    public void getUserContentForUrl(String url, Callback<UserContent> callback) {
        init();
        nyTimes.getUserContentForUrl(url, callback);
    };

    public Observable<TopStories> getTopStoriesObservable() {
        init();
        return nyTimes.getTopStoriesObservable();
    }

    public Observable<UserContent> getUserContentForUrlObservable(String url) {
        init();
        return nyTimes.getUserContentForUrlObservable(url);
    };

    public UserContent getUserContentForUrl_noCallback(String url) {
        init();
        return nyTimes.getUserContentForUrl_noCallback(url);
    };
}
