package com.mmnytimesapp.api.utils;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.mmnytimesapp.api.models.Multimedia;
import com.mmnytimesapp.api.models.Result;

import java.lang.reflect.Type;

public class ResultsDeserializerJson implements JsonDeserializer<Result> {

    @Override
    public Result deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonElement titleElement = json.getAsJsonObject().get("title");
        JsonElement multimediaElement = json.getAsJsonObject().get("multimedia");
        String section = json.getAsJsonObject().get("section").toString();
        String createdDate = json.getAsJsonObject().get("created_date").toString().replace("\"","");
        section = section.substring(1, section.length()-1);
        if (multimediaElement.isJsonArray()) {
            Result result =  new Result(
                    titleElement.toString(),
                    section,
                    createdDate,
                    (Multimedia[]) context.deserialize(multimediaElement.getAsJsonArray(), Multimedia[].class));
            result.setMyByLine(json.getAsJsonObject().get("byline").toString().replace("\"",""));
            result.setmAbstract(json.getAsJsonObject().get("abstract").toString().replace("\"",""));
            result.setmUrl(json.getAsJsonObject().get("url").toString().replace("\"",""));
            return result;
        } else if (multimediaElement.getAsString().equals("")) {
            Multimedia multimedia = new Multimedia();
            multimedia.setFormat("");
            multimedia.setUrl("");
            Result result = new Result(titleElement.toString(), section, createdDate, multimedia);
            result.setMyByLine(json.getAsJsonObject().get("byline").toString().replace("\"",""));
            result.setmAbstract(json.getAsJsonObject().get("abstract").toString().replace("\"",""));
            result.setmUrl(json.getAsJsonObject().get("url").toString().replace("\"",""));
            return result;
        } else {
            Log.d("ResultsDeserializerJson", multimediaElement.toString());
            throw new JsonParseException("Unsupported type of multimedia element");
        }
    }
}