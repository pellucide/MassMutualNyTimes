package com.mmnytimesapp.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;

import com.mmnytimesapp.R;


public class XeditText extends EditText implements OnTouchListener, View.OnFocusChangeListener,TextWatcher {

    @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
    @Override public void afterTextChanged(Editable s) { }

    public interface Listener {
        void textClearEvent();
    }

    public XeditText(Context context) {
        super(context);
        init();
    }

    public XeditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public XeditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        this.l = l;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener f) {
        this.f = f;
    }


    private Drawable xDrawable;
    private Listener listener;

    private OnTouchListener l;
    private OnFocusChangeListener f;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getCompoundDrawables()[2] != null) {
            int x = (int) event.getX();
            int y = (int) event.getY();
            int left =  getWidth() - getPaddingRight() - xDrawable.getIntrinsicWidth();
            int right =  getWidth();
            boolean tappedX = x >= left && x <= right && y >= 0 && y <= (getBottom() - getTop());
            if (tappedX) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    setText("");
                    if (listener != null) {
                        listener.textClearEvent();
                    }
                }
                return true;
            }
        }
        if (l != null) {
            return l.onTouch(v, event);
        }
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            setClearIconVisible(!TextUtils.isEmpty(getText()));
        } else {
            setClearIconVisible(false);
        }
        if (f != null) {
            f.onFocusChange(v, hasFocus);
        }
    }


    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (isFocused()) {
            setClearIconVisible(!TextUtils.isEmpty(s));
        }
    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top, Drawable right, Drawable bottom) {
        super.setCompoundDrawables(left, top, right, bottom);
        initIcon();
    }

    private void init() {
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        addTextChangedListener(this);
        initIcon();
        setClearIconVisible(false);
    }

    private void initIcon() {
        xDrawable = getCompoundDrawables()[2];
        if (xDrawable == null) {
            xDrawable = getResources().getDrawable(R.drawable.ic_clear_search);
        }
        xDrawable.setBounds(0, 0, xDrawable.getIntrinsicWidth(), xDrawable.getIntrinsicHeight());
        int min = getPaddingTop() + xDrawable.getIntrinsicHeight() + getPaddingBottom();
        if (getSuggestedMinimumHeight() < min) {
            setMinimumHeight(min);
        }
    }


    protected void setClearIconVisible(boolean visible) {
        xDrawable.setBounds(0, 0, getHeight()-30, getHeight()-30);
        Drawable[] cd = getCompoundDrawables();
        Drawable displayed = cd[2];
        boolean wasVisible = (displayed != null);
        if (visible != wasVisible) {
            Drawable x = visible ? xDrawable : null;
            super.setCompoundDrawables(cd[0], cd[1],  x, cd[3]);
        }
    }
}