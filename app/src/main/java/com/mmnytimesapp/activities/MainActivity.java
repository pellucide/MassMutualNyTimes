package com.mmnytimesapp.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mmnytimesapp.R;
import com.mmnytimesapp.adapters.ArticleAdapter;
import com.mmnytimesapp.api.models.Result;
import com.mmnytimesapp.api.models.TopStories;
import com.mmnytimesapp.bus.BusProvider;
import com.mmnytimesapp.bus.events.LoadTopStories;
import com.mmnytimesapp.bus.events.LoadedComments;
import com.mmnytimesapp.bus.events.LoadedTopStories;
import com.mmnytimesapp.fragments.ArticleFragment;
import com.mmnytimesapp.fragments.ArticlesListFragment;
import com.mmnytimesapp.ui.PagerSlidingTabStrip;
import com.mmnytimesapp.ui.XeditText;
import com.squareup.otto.Subscribe;

import java.util.List;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private android.support.v7.widget.Toolbar toolbar;
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private MyPagerAdapter adapter;
    private ArticlesListFragment articlesListFragment;
    private ListFragment categoriesListFragment;
    private ArticleAdapter articlesAdapter;
    private ListAdapter categoriesAdapter;
    XeditText xeditText;
    TextView articleTitle;
    FloatingActionButton fab;
    List<Result> results = null;
    TopStories topStories = null;

    private enum ToolbarMode {TABS, SEARCH, DETAIL}
    ToolbarMode toolbarMode=ToolbarMode.TABS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        assert toolbar != null;
        toolbar.setContentInsetsAbsolute(0,0);
        //setSupportActionBar(toolbar);
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);
        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        pager.setPageMargin(0);

        tabs.setViewPager(pager);
        tabs.setIndicatorColor(getResources().getColor(R.color.my_awesome_blue));
        tabs.setDividerColorResource(android.R.color.transparent);
        tabs.setIndicatorHeight(5);
        tabs.setDividerPadding(0);
        tabs.setShouldExpand(true);
        tabs.setTabPaddingLeftRight(0);
        tabs.setTextColorResource(R.color.my_awesome_light_blue);
        tabs.setTextSize((int)getResources().getDimension(R.dimen.tabTextSize));
        tabs.setInactiveTabAlpha(55);

        articlesListFragment = new ArticlesListFragment();
        articlesAdapter = new ArticleAdapter(this);
        articlesListFragment.setListAdapter(articlesAdapter);

        categoriesListFragment = new ListFragment();
        categoriesListFragment.setListAdapter(categoriesAdapter);;
        xeditText = (XeditText) findViewById(R.id.searchTab);
        articleTitle = (TextView) findViewById(R.id.newsTitle);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        initToolbarForTab();

        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //LayoutInflater inflater = (LayoutInflater) v.getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                //View searchView = inflater.inflate(R.layout.search_bar, null, false);
                //XeditText xeditText = new XeditText(MainActivity.this, null);
                //toolbar.removeAllViews();
                //toolbar.addView(searchView, 0, new Toolbar.LayoutParams(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT));
                //toolbar.addView(xeditText, new ViewGroup.LayoutParams(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT));

                if (toolbarMode != ToolbarMode.TABS) {
                    initToolbarForTab();
                }
                else {
                    initToolbarForSearch();
                }
            }
        });
        articlesListFragment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick position=" + position);
                if (results != null && results.size() > position)
                    initToolbarForDetail(results.get(position));
            }
        });
    }

    private void initToolbarForTab() {
        tabs.setVisibility(View.VISIBLE);
        xeditText.setVisibility(View.GONE);
        articleTitle.setVisibility(View.GONE);
        fab.setImageResource(android.R.drawable.ic_search_category_default);
        fab.setVisibility(View.VISIBLE);
        toolbarMode = ToolbarMode.TABS;
    }

    private void initToolbarForSearch() {
        tabs.setVisibility(View.GONE);
        xeditText.setVisibility(View.VISIBLE);
        articleTitle.setVisibility(View.GONE);
        fab.setImageResource(R.drawable.close);
        fab.setVisibility(View.VISIBLE);
        toolbarMode = ToolbarMode.SEARCH;
    }

    private void initToolbarForDetail(Result result) {
        tabs.setVisibility(View.GONE);
        xeditText.setVisibility(View.GONE);
        articleTitle.setVisibility(View.VISIBLE);
        articleTitle.setText(result.getTitle());
        articleTitle.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                MainActivity.this.onBackPressed();
                                            }
                                        });
        fab.setVisibility(View.GONE);
        toolbarMode=ToolbarMode.DETAIL;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ArticleFragment articleFragment = ArticleFragment.getInstance();
        articleFragment.setResult(result);
        ft.replace(R.id.content, articleFragment).addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(toolbarMode == ToolbarMode.DETAIL) {
            initToolbarForTab();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        BusProvider.getInstance().register(this);
        BusProvider.getInstance().post(new LoadTopStories());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] TITLES = { "Articles", "Categories", "Settings" };

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            Log.e(TAG, "getItem(position="+position+")");
            if (position == 0) {
                //return ArticleFragment.getInstance();
                //return new ListFragment();
                return articlesListFragment;
            }
            if (position == 1) {
                return new ListFragment();
            }
            return new Fragment();
        }
    }

    /***
     *  Otto Subscription Methods
     */

    @Subscribe
    public void onLoadedTopStories(LoadedTopStories loadedTopStories) {
        Log.d(TAG, "onLoadedTopStories");
        topStories = loadedTopStories.getTopStories();
        results = topStories.getResults();
        articlesAdapter.setResults(results);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                articlesAdapter.notifyDataSetChanged();
                articlesListFragment.getListView().invalidate();
            }
        });
    }


    @Subscribe
    public void onLoadedComments(final LoadedComments loadedComments) {
        Log.d(TAG, "onLoadedCommencts");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                articlesAdapter.notifyDataSetChanged();
                ListView lv = articlesListFragment.getListView();
                lv.invalidate();
            }
        });
    }
}
