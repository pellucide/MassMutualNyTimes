package com.mmnytimesapp.delegate;

import android.content.Context;
import android.util.Log;

import com.mmnytimesapp.api.NYTimesService;
import com.mmnytimesapp.api.models.Result;
import com.mmnytimesapp.api.models.TopStories;
import com.mmnytimesapp.api.models.UserContent;
import com.mmnytimesapp.bus.events.LoadTopStories;
import com.mmnytimesapp.bus.events.LoadedComments;
import com.mmnytimesapp.bus.events.LoadedTopStories;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.Notification;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static java.lang.String.format;

public class NYTimesDelegate {

    private static final String TAG = NYTimesDelegate.class.getSimpleName();

    private NYTimesService sClient;
    private Context mContext;
    private Bus mBus;

    public NYTimesDelegate(Context context, Bus bus) {
        this.mContext = context;
        this.mBus = bus;
        this.sClient = NYTimesService.getInstance();
    }

    //@Subscribe
    public void onLoadTopStoriesOld(LoadTopStories loadTopStories) {
        Log.d(TAG, "onLoadTopStories");

        final Callback<TopStories> callback = new Callback<TopStories>() {
            @Override
            public void success(TopStories topStories, Response response) {
                Log.d(TAG, "onSuccess");
                mBus.post(new LoadedTopStories(topStories));
                int index=0;
                for(Result result: topStories.getResults()) {
                    final Result fResult = result;
                    sClient.getUserContentForUrl(result.getUrl(), new Callback<UserContent>() {
                        @Override
                        public void success(UserContent userContent, Response response) {
                            fResult.mUserContent = userContent;
                            Log.e(TAG, "result.url="+ fResult.getUrl() + "returned OK");
                        }

                        @Override public void failure(RetrofitError error) { Log.e(TAG, "result.url==="+ fResult.getUrl() + "  returned "+error);}
                    });
                    index++;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG, error.getMessage());
                Log.d(TAG, error.getResponse().getReason());
            }
        };

        sClient.getTopStories(callback);
    }

    @Subscribe
    public void onLoadTopStories(LoadTopStories loadTopStories) {
        Log.d(TAG, "onLoadTopStoriesObservable");
        sClient.getTopStoriesObservable()
               .map(new Func1<TopStories, List<Result>>() {
                   @Override
                   public List<Result> call(TopStories topStories) {
                       Log.d(TAG,  "---test1----");
                       mBus.post(new LoadedTopStories(topStories));
                       return topStories.getResults();
                   }
               })
               .flatMap(new Func1<List<Result>, Observable<Result>>() {
                   @Override
                   public Observable<Result> call(List<Result> results) {
                       Log.d(TAG,  "---test2----");
                       return Observable.from(results);
                   }
               })

               //.flatMapIterable(new Func1<List<Result>, Iterable<Result>>() {
                    //@Override
                    //public Iterable<Result> call(List<Result> results) {
                        //Log.d(TAG,  "---test2----");
                        //return results;
                    //}
               //})
               //.subscribeOn(Schedulers.newThread())
               //.observeOn(AndroidSchedulers.mainThread())
               .concatMap(new Func1<Result, Observable<Result>>() {
                    @Override
                    public Observable<Result> call(Result result) {
                        return Observable.just(result).delay(250, TimeUnit.MILLISECONDS);
                    }
               })
               //.throttleLast(250, TimeUnit.MILLISECONDS)
               //.map(new Func1<Result, UserContent>() {
                    //@Override
                    //public UserContent call(Result result) {
                        //Log.d(TAG,  "---test3----");
                        //return null;
                    //}
               //})
               //.subscribe(new Observer<List<Result>>() {
                    //@Override
                    //public void onCompleted() {
                        //Log.d(TAG,"Retrofit call 1 completed");
                    //}

                    //@Override
                    //public void onError(Throwable e) {
                        //Log.e(TAG, "woops we got an error while getting the list of contributors");
                    //}

                    //@Override
                    //public void onNext(List<Result> results) {
                        //for (Result r : results) {
                            //Log.d(TAG,"url="+r.getUrl());
                        //}
                    //}
                //});
                .subscribe(new Observer<Result>() {
                    int count=0;
                    @Override
                    public void onCompleted() {
                        Log.d(TAG,"Retrofit call 1 completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "woops we got an error ");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Result result) {
                        Log.d(TAG, "count="+count +" result.url="+result.getUrl());
                        result.mUserContent = sClient.getUserContentForUrl_noCallback(result.getUrl());
                        Log.d(TAG, "result.mUserContent.getTimeStamp()="+ result.mUserContent.getResult().getTimeStamp());
                        mBus.post(new LoadedComments(count));
                        count++;
                    }
                });
    }
}
