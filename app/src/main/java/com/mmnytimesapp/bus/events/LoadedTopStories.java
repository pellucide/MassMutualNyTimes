package com.mmnytimesapp.bus.events;

import com.mmnytimesapp.api.models.TopStories;

public class LoadedTopStories {

    private TopStories mTopStories;

    public LoadedTopStories(TopStories topStories) {
        this.mTopStories = topStories;
    }

    public TopStories getTopStories() {
        return mTopStories;
    }

}
