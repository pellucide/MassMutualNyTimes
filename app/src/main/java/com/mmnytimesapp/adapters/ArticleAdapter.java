package com.mmnytimesapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.mmnytimesapp.R;
import com.mmnytimesapp.api.models.Multimedia;
import com.mmnytimesapp.api.models.Result;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;

public class ArticleAdapter extends BaseAdapter {

    public static final String TAG = ArticleAdapter.class.getSimpleName();
    private static final String NORMAL = "Normal";
    public static int VIEW_HOLDER_INDEX=R.string.viewholder;

    private Context mContext;
    private List<Result> mResults;
    SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy hh:mma");

    public ArticleAdapter(Context context) {
        this.mContext = context;
    }

    public void setResults(List<Result> results) {
        this.mResults = results;
    }
    public Result getResults(int position) {
        return mResults.get(position);
    }
    public int getItemCount() {
        if (mResults != null) {
            return mResults.size();
        }
        return 0;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "ArticleAdapter.getCount()="+getItemCount());
        return getItemCount();
    }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ArticleViewHolder articleViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.article_list_item, parent, false);
            mContext = convertView.getContext();
            articleViewHolder = new ArticleViewHolder(convertView);
            convertView.setTag(VIEW_HOLDER_INDEX, articleViewHolder);
        }
        else {
            articleViewHolder = (ArticleViewHolder) convertView.getTag(VIEW_HOLDER_INDEX);
        }

        Result result = mResults.get(position);
        String section = result.getSection();
        //Log.d(TAG,"section="+section);
        //Log.d(TAG,"Url="+result.getUrl());
        int imageResource = 0;

        if (TextUtils.isEmpty(section)) {
            section = "unknown";
        }

        if (section.equalsIgnoreCase("business")) {
            imageResource = R.drawable.ic_business;
        }

        if (section.equalsIgnoreCase("sports")) {
            imageResource = R.drawable.ic_sports;
        }

        if (section.equalsIgnoreCase("arts")) {
            imageResource = R.drawable.ic_arts;
        }

        if (section.equalsIgnoreCase("politics")) {
            imageResource = R.drawable.ic_politics;
        }

        if (section.equalsIgnoreCase("science")) {
            imageResource = R.drawable.ic_science;
        }

        if (imageResource != 0) {
            articleViewHolder.mArticleImage.setImageResource(imageResource);
        } else {
            // Set Image
            List<Multimedia> multimediaList = result.getMultimedia();
            for (int i = 0, size = multimediaList.size(); i < size; i++) {
                //Log.d(TAG, multimediaList.get(i).getFormattedUrl());
                if (multimediaList.get(i).getFormat().contains(NORMAL)) {
                    String url = multimediaList.get(i).getFormattedUrl();
                    Picasso.with(mContext)
                            .load(url)
                            .fit()
                            .centerCrop()
                            .into(articleViewHolder.mArticleImage);
                }
            }
        }

        // Set Title
        String title = result.getTitle();
        Log.d(TAG, title);
        if (title != null)
            articleViewHolder.mArticleTitle.setText(title);
        if (result.getCreatedDate() != null)
            articleViewHolder.mCreatedDate.setText(sdf.format(result.getCreatedDate()));

        if (result.mUserContent != null) {
            // Set comments count
            int commentsCount = result.mUserContent.getResult().totalCommentsFound;
            Log.d(TAG, "comments = "+commentsCount);
            articleViewHolder.mComments.setText("");
            if (commentsCount != 0)
                articleViewHolder.mComments.setText(String.valueOf(commentsCount));

            // Set likes count
            int likesCount = result.mUserContent.getResult().totalRecommendationsFound;
            Log.d(TAG, "likes = "+likesCount);
            articleViewHolder.mLikes.setText("");
            if (likesCount != 0)
                articleViewHolder.mLikes.setText(String.valueOf(likesCount));
        }
        return convertView;
    }

    private class ArticleViewHolder extends RecyclerView.ViewHolder {
        private ImageView mArticleImage;
        private TextView mArticleTitle;
        private TextView mCreatedDate;
        private TextView mLikes;
        private TextView mComments;

        public ArticleViewHolder(View itemView) {
            super(itemView);
            mArticleImage = (ImageView) itemView.findViewById(R.id.articleImage);
            mArticleTitle = (TextView) itemView.findViewById(R.id.articleTitle);
            mCreatedDate = (TextView) itemView.findViewById(R.id.articleDate);
            mComments = (TextView) itemView.findViewById(R.id.articleComments);
            mLikes = (TextView) itemView.findViewById(R.id.articleLikes);
        }
    }
}