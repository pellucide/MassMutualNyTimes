package com.mmnytimesapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mmnytimesapp.R;
import com.mmnytimesapp.api.models.Multimedia;
import com.mmnytimesapp.api.models.Result;
import com.mmnytimesapp.api.models.UserContent;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CommentsAdapter extends BaseAdapter {

    public static final String TAG = CommentsAdapter.class.getSimpleName();
    private static final String NORMAL = "Normal";
    public static int VIEW_HOLDER_INDEX=R.string.viewholder;

    private Context mContext;
    private List<UserContent.Comment> mComments;
    SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy hh:mma");

    public CommentsAdapter(Context context) {
        this.mContext = context;
    }

    public CommentsAdapter(Context context, List<UserContent.Comment> comments) {
        this.mContext = context;
        this.setComments(comments);
    }

    public void setComments(List<UserContent.Comment> comments) {
        this.mComments = comments;
    }
    public UserContent.Comment getResults(int position) {
        return mComments.get(position);
    }
    public int getItemCount() {
        if (mComments != null) {
            return mComments.size();
        }
        return 0;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "CommentAdapter.getCount()="+getItemCount());
        return getItemCount();
    }

    @Override
    public Object getItem(int position) { return null; }

    @Override
    public long getItemId(int position) { return 0; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommentViewHolder commentViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.comment_list_item, parent, false);
            mContext = convertView.getContext();
            commentViewHolder = new CommentViewHolder(convertView);
            convertView.setTag(VIEW_HOLDER_INDEX, commentViewHolder);
        }
        else {
            commentViewHolder = (CommentViewHolder) convertView.getTag(VIEW_HOLDER_INDEX);
        }

        UserContent.Comment comment = mComments.get(position);
        String imageResource = comment.picURL;

        Log.d(TAG, "imageResource:"+imageResource);
                    Picasso.with(mContext)
                            .load(imageResource)
                            .fit()
                            .centerCrop()
                            .into(commentViewHolder.mCommentImage);

        // Set Title
        String body = comment.commentBody;
        Log.d(TAG, body);
        if (body != null)
            commentViewHolder.mCommentTitle.setText(Html.fromHtml(body));

        Date commentDate = new Date();
        commentDate.setTime(Long.valueOf(comment.createDate) * 1000);
        if (commentDate != null);
            commentViewHolder.mCreatedDate.setText(sdf.format(commentDate));

        return convertView;
    }

    private class CommentViewHolder extends RecyclerView.ViewHolder {
        private ImageView mCommentImage;
        private TextView mCommentTitle;
        private TextView mCreatedDate;

        public CommentViewHolder(View itemView) {
            super(itemView);
            mCommentImage = (ImageView) itemView.findViewById(R.id.userImage);
            mCommentTitle = (TextView) itemView.findViewById(R.id.commentText);
            mCreatedDate = (TextView) itemView.findViewById(R.id.commentDate);
        }
    }
}