package com.mmnytimesapp.fragments;

import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by jpbrahma on 6/13/16.
 */
public class ArticlesListFragment extends ListFragment {
    AdapterView.OnItemClickListener listener=null;

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (listener != null) {
            listener.onItemClick(l,v,position,id);
        }
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
        this.listener = listener;
    }
}
