package com.mmnytimesapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.mmnytimesapp.R;
import com.mmnytimesapp.adapters.CommentsAdapter;
import com.mmnytimesapp.api.models.Multimedia;
import com.mmnytimesapp.api.models.Result;
import com.mmnytimesapp.bus.events.LoadedComments;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ArticleFragment extends Fragment {
    private static final String TAG=ArticleFragment.class.getSimpleName();
    private static ArticleFragment mArticleFragment;
    private Result result;
    private TextView createdDate, author, likeCount, abstractText, articleImageCaption;
    private ImageView articleImage;
    private LinearLayout commentsList;
    private CommentsAdapter commentsAdapter;

    public static ArticleFragment getInstance() {
        if (mArticleFragment == null)
            return mArticleFragment = new ArticleFragment();
        return mArticleFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_articles, null);
        bindData(root);
        return root;
    }

    private void initViews(View view) {
        createdDate = (TextView) view.findViewById(R.id.createdDate);
        author = (TextView) view.findViewById(R.id.author);
        likeCount = (TextView) view.findViewById(R.id.likeCount);
        abstractText = (TextView) view.findViewById(R.id.abstractText);
        articleImageCaption = (TextView) view.findViewById(R.id.articleImageCaption);
        articleImage = (ImageView) view.findViewById(R.id.articleImage);
        commentsList = (LinearLayout) view.findViewById(R.id.commentsList);
    }

    private void bindData(View view) {
        initViews(view);

        createdDate.setText(result.getCreatedDate().toString());
        author.setText(result.getMyByLine());
        abstractText.setText(result.getAbstract());
        if (result.mUserContent != null)
            likeCount.setText(""+result.mUserContent.getResult().totalRecommendationsFound);
        else
            likeCount.setText("");
        // Set Image
        List<Multimedia> multimediaList = result.getMultimedia();
        for (int i = 0, size = multimediaList.size(); i < size; i++) {
            Log.d(TAG, multimediaList.get(i).getFormattedUrl());
            //if (multimediaList.get(i).getFormat().toLowerCase().contains("superjumbo")) {
                String url = multimediaList.get(i).getFormattedUrl();
                Log.d(TAG, "selected="+ multimediaList.get(i).getFormattedUrl());
                articleImageCaption.setText(multimediaList.get(i).getCaption());
                if (!TextUtils.isEmpty(url)) {
                    Picasso.with(getActivity())
                            .load(url)
                            .into(articleImage);
                }
            //}
        }

        if (this.result != null)
            setupList();
    }

    public void setResult(Result result) {
        this.result = result;
        setupList();
    }

    private void setAdapter() {
        if (commentsAdapter != null && commentsList != null) {
            View v;
            int ii;
            commentsList.removeAllViews();
            for (ii=0; ii<commentsAdapter.getItemCount(); ii++) {
                commentsList.addView(commentsAdapter.getView(ii,null, commentsList));
            }
        }
    }

    private void setupList() {
        if (result.mUserContent != null){
            commentsAdapter = new CommentsAdapter(this.getActivity(), result.mUserContent.getResult().comments);
            setAdapter();
        }
    }

    @Subscribe
    public void onLoadedComments(final LoadedComments loadedComments) {
        Log.d(TAG, "onLoadedCommencts");
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                commentsAdapter = new CommentsAdapter(getActivity(), result.mUserContent.getResult().comments);
                setAdapter();
                //commentsAdapter.notifyDataSetChanged();
                commentsList.invalidate();
            }
        });
    }
}
