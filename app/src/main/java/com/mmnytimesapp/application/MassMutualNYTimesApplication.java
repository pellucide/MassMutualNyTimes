package com.mmnytimesapp.application;

import android.app.Application;

import com.mmnytimesapp.bus.BusProvider;
import com.mmnytimesapp.bus.events.LoadTopStories;
import com.mmnytimesapp.delegate.NYTimesDelegate;
import com.squareup.otto.Bus;

public class MassMutualNYTimesApplication extends Application {

    private Bus mBus = BusProvider.getInstance();
    private NYTimesDelegate mNYTimesDelegate;

    @Override
    public void onCreate() {
        super.onCreate();
        mNYTimesDelegate = new NYTimesDelegate(this, mBus);
        mBus.register(this);
        mBus.register(mNYTimesDelegate);
        //mBus.post(new LoadTopStories());
    }
}
